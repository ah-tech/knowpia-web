import React, { Component } from 'react'
import styled from 'styled-components';
import One from '../assets/1.svg'
import forward from '../assets/forward.svg'
import view from '../assets/view.svg'
import save from '../assets/save.svg'
import love from '../assets/love.svg'
import comment from '../assets/comment.svg'
const Container = styled.div`
width:100%;
border-bottom:1px solid #C1C2C4;
@media(min-width:768px){
display:flex;
justify-content:space-between;
}
@media(max-width: 768px) and (min-width: 455px){
    display:flex;
    justify-content:space-between;    
}
@media(max-width:455px){
    display:flex;
    flex-direction:column;
    flex:1;
    justify-content:space-between;    

}
`

const Image = styled.img`
&.react{
    @media(min-width:768px){
        width:18px;
        margin-right:12px;
        margin-left:8px;
    }    
    @media(max-width: 768px) and (min-width: 455px){
        width:12px;
        margin-right:5px;
        margin-left:5px;        
    }   
    @media(max-width:455px){
        width:12px;
        margin-right:5px;
        margin-left:5px;        
    }
}
&#post{
    @media(min-width:768px){
        width:200px;
        margin-right:25px
    }    
    @media(max-width: 768px) and (min-width: 455px){
        width:200px;
        margin-right:25px
        
    }   
    @media(max-width:455px){
        width:200px;        
    }
}
`
const DIV = styled.div`
&.left{
float:left
}
&.right{
float:right;
@media(min-width:768px){
}
@media(max-width: 768px) and (min-width: 455px){
}
@media(max-width:455px){
}
}
&.post-div{
    @media(min-width:768px){
        width:80%;
        display:inline
    }    
    @media(max-width: 768px) and (min-width: 455px){
        max-width:80%;
        display:inline        
    }   
    @media(max-width:455px){
    }

}
`
const H1 = styled.h2`
@media(min-width:768px){
    color:#2C96D4
}    
@media(max-width: 768px) and (min-width: 455px){
    font-size:20px
    color:#2C96D4
    
}   
@media(max-width:455px){
    color:#2C96D4    
}

`
const P = styled.p`
@media(min-width:768px){
    color:#4F555C
}    
@media(max-width: 768px) and (min-width: 455px){
    color:#4F555C;
    font-size:16px
    
}   
@media(max-width:455px){
    color:#4F555C;
    font-size:12px    
    
}
`
const Span = styled.span`
&.left{
    @media(min-width:768px){
        font-size:16px
    }    
    @media(max-width: 768px) and (min-width: 455px){
        font-size:12px        
    }   
    @media(max-width:455px){
        font-size:10px
        
    }
}
&.right{
    @media(min-width:768px){
        font-size:16px;
        color: #BFD4DA;
        margin-right: 40px; 
        margin-bottom: 4px; 
        margin-left: -2px
    }    
    @media(max-width: 768px) and (min-width: 455px){
    font-size:12px;
    color: #BFD4DA;
    margin-right: 10px; 
    margin-bottom: 4px; 
    margin-left: -2px

    }   
    @media(max-width:455px){
        font-size:10px;
        color: #BFD4DA;
        margin-right: 20px; 
        margin-bottom: 4px; 
        margin-left: -2px

    }
}
`
class Post extends Component {
    render() {
        return (
            <Container>
                <Image src={One} id="post" />
                <DIV className="post-div">
                <H1>
                        Ivanka Trump says The media is not the enemy of the people
                </H1>
                <P>
                        Washington (CNN) Ivanka Trump said Thursday She Does not believe that the press
                        is the enemy of the people
                </P>
                    <DIV className="lower">
                        <DIV className="left">
                            <Span style={{ color: "#2C96D4" }} className="left">Bulan Juni</Span>
                            <Span style={{ color: "#4F555C" }} className="left">- Yesterday 23:44</Span>
                        </DIV>
                        <DIV className="right">
                            <Image className="react" src={love} />
                            <Span
                                className="right"
                            >24</Span>
                            <Image className="react" src={comment} />
                            <Image className="react" style={{ marginBottom: 3.5 }} src={view} />
                            <Span
                                className="right"
                            >2000</Span>
                            <Image className="react" src={forward} />
                            <Image className="react" src={save} />
                        </DIV>
                    </DIV>
                </DIV>
            </Container>
        )
    }
}
export default Post;