import React, { Component } from 'react';
import styled from 'styled-components';
import Logo from '../assets/icon.svg'
import Youtube from '../assets/youtube.svg'
import FB from '../assets/fb.svg'
import Twitter from '../assets/twitter.svg'
import LinkedIn from '../assets/linkedin.svg'

const Container = styled.div`
width:100%;
margin:20px
`
const Image = styled.img`
&.logo{
width:20px;
}
&.sociel{
width:25px;
height:25px;
margin:5px
}
`
const DIV = styled.div`
&.nav{
    @media(min-width:768px){
        width:40%;
        margin-right:auto;
        margin-left:auto;
        display:flex;
        flex:1;
        justify-content:space-between;
        margin-top:20px;        
        margin-bottom:20px;
        font-family:Calibri;
        font-weight:bold
    }
    @media(max-width: 768px) and (min-width: 455px){
        width:60%;
        margin-right:auto;
        margin-left:auto;
        display:flex;
        flex:1;
        justify-content:space-between;
        margin-top:20px;        
        margin-bottom:20px;        
        font-family:Calibri;
        font-weight:bold
    }
    @media(max-width:455px){
        display:none
    }
}
&.terms{
    @media(min-width:768px){
        width:35%;
        margin-right:auto;
        margin-left:auto;
        color:#4F555C;
        text-align:center;
        font-size:14px
    }
    @media(max-width: 768px) and (min-width: 455px){
        width:50%;
        margin-right:auto;
        margin-left:auto;
        color:#4F555C;
        text-align:center;
        font-size:12px
                
    }
    @media(max-width:455px){
        width:75%;
        margin-right:auto;
        margin-left:auto;
        color:#4F555C;
        text-align:center;
        font-size:10px
                
    }
}

`
const Span = styled.span`

`
const Footer = () => (
    <Container>

        <div style={{ textAlign: "center" }}>
            <Image src={Logo} className="logo" />
        </div>
        <DIV className="nav">
            <span>ABOUT</span>
            <span>PRIVACY</span>
            <span>CONTACT</span>
            <span>FAQ</span>
        </DIV>
        <DIV className="terms">
            By using this platform you agree to the <span style={{ color: "#2C96D4" }}>Terms of use</span>
            {'\n'}©2018 KNOWPIA
        </DIV>
        <div style={{ width: "140px", marginRight: "auto", marginLeft: "auto" }}>
            <Image src={FB} className="sociel" />
            <Image src={LinkedIn} className="sociel" />
            <Image src={Twitter} className="sociel" />
            <Image src={Youtube} className="sociel" />
        </div>

    </Container>
)
export default Footer