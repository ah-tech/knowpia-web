import React, { Component } from 'react';
import styled from 'styled-components'
import ProfilePhoto from '../assets/profilePhoto.svg'
import Followers from '../assets/followers.svg'
import Follow from '../assets/follow.svg'
import Youtube from '../assets/youtube.svg'
import FB from '../assets/fb.svg'
import Twitter from '../assets/twitter.svg'
import LinkedIn from '../assets/linkedin.svg'
import Header from './header'
const Container = styled.div`
@media(min-width:768px){
    width:100%
}
@media(max-width: 768px) and (min-width: 455px){
    width:100%

}
@media(max-width:455px){
    width:100%
}
`
const ColoredTop = styled.div`
height: 100px;
background-color:#A8DDE0
width:100%;
@media(min-width:768px){
    display:flex;
    margin-top:-20px;
    justify-content:space-between
}
`
const ProfilePic = styled.img`
width:140px;
height:140px;
margin-top:-70px;
`
const Image = styled.img`
&#follow-image{
width:80px;
}
&.sociel{
width:25px;
height:25px;
margin:5px
}
&#followersIMG{
    @media(max-width: 768px) and (min-width: 455px){
    height:40px;
    width:40px    
    }
    @media(min-width:768px){
    height:40px;
    width:40px    
}
    @media(max-width:455px){
    height:35px;
    width:35px    
    }
}
`
const DIV = styled.div`
&#extra{
    @media(min-width:768px){
        width:20%;
        height:50px;
    }    
    @media(max-width: 768px) and (min-width: 455px){
        display:none
    }   
    @media(max-width:455px){
        display:none
    }
}
/*
&#topleft{
    @media(min-width:768px){
    }    
    @media(max-width: 768px) and (min-width: 455px){
        display:none
    }   
    @media(max-width:455px){
        display:none
    }
}
&#topright{
    @media(min-width:768px){
    }    
    @media(max-width: 768px) and (min-width: 455px){
        display:none
    }   
    @media(max-width:455px){
        display:none
    }     
}
*/
&#overview{
    @media(min-width:768px){
      width:250px;
      font-size:14px;
      margin:20px
    }    
    @media(max-width: 768px) and (min-width: 455px){
    display:none
    }   
    @media(max-width:455px){
    display:none        
    }     
}&#overview-mob{
    @media(min-width:768px){
      display:none
    }    
    @media(max-width: 768px) and (min-width: 455px){
        width:250px;
        font-size:14px;
        text-align:center;
        margin-left:auto;
        margin-right:auto
      }   
    @media(max-width:455px){
        width:250px;
        font-size:14px;
        text-align:center;
        margin-left:auto;
        margin-right:auto
    }     
}
&.followersDIV{
    @media(min-width:768px){
        width:40px;
        right:40px;
        margin:20px
        display:inline;
    }    
    @media(max-width: 768px) and (min-width: 455px){
        width:40px;
        margin:10px
        
    }   
    @media(max-width:455px){
        width:40px;
        margin:15px    
    }     
}
&#followersDIV{
    @media(min-width:768px){
    float:right      
    }    
    @media(max-width: 768px) and (min-width: 455px){
        float:right
    }   
    @media(max-width:455px){
        float:right    
    }     
}
&#followingDIV{
    @media(min-width:768px){
    float:right      
    }    
    @media(max-width: 768px) and (min-width: 455px){
        float:left      
    }   
    @media(max-width:455px){
        float:left    
    }     
}

`
const Span = styled.span`
&.follow{
    @media(min-width:768px){
        color:white;
        font-size:12px
    }    
    @media(max-width: 768px) and (min-width: 455px){
        color:white;
        font-size:12px        
    }   
    @media(max-width:455px){
        color:white;
        font-size:10px
        
    }     
}

`
class ProfileView extends Component {
    render() {
        return (
            <Container>
                <div style={{ backgroundColor: "#A8DDE0" }}>
                    <Header />
                    <ColoredTop>
                        <DIV id="overview">
                            There are numerous investment options present in the market in which people may
                            invest their surplus to earn the time value of the money
                </DIV>
                        <DIV id="extra"></DIV>
                        <DIV id="followersDIV" className="followersDIV" >
                            <Image id="followersIMG" src={Followers} />
                            <Span className="follow">Followers{'\n'}</Span>
                            <Span>488</Span>

                        </DIV>
                        <DIV id="followingDIV" className="followersDIV">
                            <Image id="followersIMG" src={Followers} />
                            <Span className="follow">Following{'\n'}</Span>
                            <Span>2,009</Span>
                        </DIV>
                    </ColoredTop>
                </div>
                <div style={{ display: "flex", flex: 1, justifyContent: "center" }}>
                    <ProfilePic src={ProfilePhoto} />
                </div>
                <h1
                    style={{ textAlign: "center", margin: 0 }}
                >Tiffany Lainon</h1>
                <DIV id="overview-mob">
                    There are numerous investment options present in the market in which people may
                    invest their surplus to earn the time value of the money
                </DIV>
                <div style={{ width: "140px", marginRight: "auto", marginLeft: "auto" }}>
                    <Image src={FB} className="sociel" />
                    <Image src={LinkedIn} className="sociel" />
                    <Image src={Twitter} className="sociel" />
                    <Image src={Youtube} className="sociel" />
                </div>

                <div style={{ textAlign: "center" }}>
                    <Image src={Follow} id="follow-image" />
                </div>
            </Container>
        )
    }
}
export default ProfileView