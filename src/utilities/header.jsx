import React, { Component } from 'react';
import styled from 'styled-components';
import Logo from '../assets/logo.svg'
import Burger from '../assets/burger.svg'
import Add from '../assets/add.svg'
import Bell from '../assets/bell.svg'
import Lang from '../assets/lang.svg'
import Photo from '../assets/photo.svg'
import Search from '../assets/searchBox.svg'

const Container = styled.div`
background-color:#A8DDE0;
min-height : 40px;
width:100%;
@media(min-width:768px){
}
@media(max-width: 768px) and (min-width: 455px){

}
@media(max-width:455px){

}
`
const OptionsLeft = styled.div`
@media(min-width:768px){
    dispay:inline;
    max-width:12%;
    float:left;
    margin:10px
}
@media(max-width: 768px) and (min-width: 455px){
display:none;!important
width:0px

}
@media(max-width:455px){
display:none;!important
width:0px
}
`

const Icon = styled.img`
@media(min-width:768px){
    float:left;
    max-width:100px;
    max-height:50px;
    margin:10px
}
@media(max-width: 768px) and (min-width: 455px){
    float:left;
    max-width:80px;
    max-height:40px;
    margin:10px
}
@media(max-width:455px){
    float:left;
    max-width:80px;
    max-height:40px;
    margin:10px
}
`
const BurgerIcon = styled.img`
@media(min-width:768px){
display:none!important;
width:0px
}
@media(max-width: 768px) and (min-width: 455px){
    max-width:24px;
    max-height:12px;
    float:left;
    margin-left:5px;
    margin-top:15px;
    margin-bottom:15px;
}
@media(max-width:455px){
    max-width:24px;
    max-height:12px;
    float:left;
    margin-left:5px;
    margin-top:15px;
    margin-bottom:15px;

}
`
const RightImages = styled.img`
&.lang{
    @media(max-width: 768px) and (min-width: 455px){
    display:none;important
    width:0px
    }    
}
@media(min-width:768px){
    max-width:40px;
    max-height:20px;
    float:right;
    margin-top:10px;
    margin-bottom:10px;
    }
    @media(max-width: 768px) and (min-width: 455px){
        max-width:40px;
        max-height:20px;
        float:right;
        margin-top:10px;
        margin-bottom:10px;
    }
    @media(max-width:455px){
        display:none;!important
        width:0px
/*        max-width:24px;
        max-height:12px;
        float:left;
        margin-left:5px;
        margin-top:15px;
        margin-bottom:15px;
  */  
    }
    
`
const SearchContainer =
    styled.img`
@media(min-width:768px){
    height:40px;
    float:right;
    margin-left:5px;

    }
    @media(max-width: 768px) and (min-width: 455px){
        height:40px;
        float:right;
        margin-left:5px;
        
        }
    @media(max-width:455px){
        height:40px;
        float:right;
        margin-left:5px;
        
        }
    
`
class Header extends Component {
    render() {
        return (
            <Container>
                <BurgerIcon src={Burger} />
                <Icon src={Logo} />
                <OptionsLeft>KnowPedia</OptionsLeft>
                <OptionsLeft>Blog</OptionsLeft>
                <OptionsLeft>How it Works</OptionsLeft>
                <SearchContainer src={Search} />
                <RightImages className="lang" src={Lang} />
                <RightImages src={Add} />
                <RightImages src={Photo} />
                <RightImages src={Bell} />
            </Container>
        )
    }
}
export default Header