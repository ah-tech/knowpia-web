import React ,{Component} from 'react';
import Header from '../utilities/header'
import ProfileView from '../utilities/profileView'
import Post from '../utilities/post'
import Footer from '../utilities/footer'
class UserPublicProfile extends Component{
    render(){
        return(
            <div>

                {/* <Header/> */}
                <ProfileView/>
                {[1,2,3,4,5].map(
                    (data,index)=>
                (<Post key={index}/>)
                )}
                <Footer/>
            </div>
        )
    }
}
export default UserPublicProfile